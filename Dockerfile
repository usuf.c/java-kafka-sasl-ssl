FROM openjdk:8-jdk-alpine

ARG KAFKA_SECURITY=${KAFKA_SECURITY}
ARG JAR=${JAR}

ENV KAFKA_OPTS -Djava.security.auth.login.config=/app/client-jaas.conf
RUN apk add --no-cache tzdata
ENV TZ Asia/Bangkok
RUN mkdir app
WORKDIR /app/

ADD /${KAFKA_SECURITY}/* /app/
ADD /${KAFKA_SECURITY}/krb5.conf /etc/krb5.conf
ADD ${JAR} app.jar

CMD java $JVM_OPTS -Djava.security.auth.login.config=/app/client-jaas.conf -jar app.jar
