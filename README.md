# java-kafka-sasl-ssl
- **map host**
    172.16.3.141 kkts01
    172.16.3.142 kkts02
    172.16.3.143 kkts03
    172.16.3.245 napat-test
- **update config**
    - **client-jaas.conf**
        - keyTab="kt.keytab.keytab"
        - principal="your-user@KAFKA.SECURE"; 
- **execute jar**
   java -Djava.security.auth.login.config=client_jaas.conf -Djava.security.krb5.conf=krb5.conf -jar java-kafka-sasl-ssl-1.0-SNAPSHOT.jar
