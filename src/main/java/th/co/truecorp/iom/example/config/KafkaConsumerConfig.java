package th.co.truecorp.iom.example.config;

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import th.co.truecorp.iom.example.consumer.SerializationExceptionHandler;
import th.co.truecorp.iom.example.model.NotificationMessage;

import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableKafka
public class KafkaConsumerConfig {

    @Value("${kafka.bootstrap.servers}")
    private String bootstrapServers;
    @Value("${iom.notification.consumer.group}")
    private String groupId;
    @Value("${kafka.consumer.retry.idle.interval.milliseconds:30000}")
    private Integer maxPollRecords;
    @Value("${kafka.consumer.max.poll.records:100}")
    private String consumerRetryIdleInterval;
    @Value("${kafka.consumer.concurrent:3}")
    private Integer consumerConcurrent;

    @Value("${kafka.security.protocol:SSL}")
    private String kafkaSecurityProtocol;
    @Value("${kafka.security.truststore.location}")
    private String trustStoreLocation;
    @Value("${kafka.security.truststore.password}")
    private String trustStorePassword;
    @Value("${kafka.security.kerberos.service.name}")
    private String kafkaSecurityKerberosServiceName;

    @Bean(name = "kafkaConsumerConfigs")
    public Map<String, Object> kafkaConsumerConfigs() {

        Map<String, Object> props = new HashMap<>();
        if ("SASL_SSL".equals(kafkaSecurityProtocol)) {
            props.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, "SASL_SSL");
            props.put("sasl.mechanism", "GSSAPI");
            props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, trustStoreLocation);
            props.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, trustStorePassword);
            props.put("sasl.kerberos.service.name", kafkaSecurityKerberosServiceName);
        } else if ("SSL".equals(kafkaSecurityProtocol)) {
            props.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, "SSL");
            props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, trustStoreLocation);
            props.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, trustStorePassword);
        }

        // list of host:port pairs used for establishing the initial connections
        // to the Kakfa cluster
        props.put(org.apache.kafka.clients.consumer.ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.put(org.apache.kafka.clients.consumer.ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(org.apache.kafka.clients.consumer.ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        // consumer groups allow a pool of processes to divide the work of
        // consuming and processing records
        props.put(org.apache.kafka.clients.consumer.ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.put(org.apache.kafka.clients.consumer.ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        props.put(org.apache.kafka.clients.consumer.ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
        props.put(org.apache.kafka.clients.consumer.ConsumerConfig.MAX_POLL_RECORDS_CONFIG, maxPollRecords);

        //*** earliest: automatically reset the offset to the earliest offset
        //*** latest: automatically reset the offset to the latest offset
        //*** none: throw exception to the consumer if no previous offset is found for the consumer's group

        return props;
    }

    @Bean(name = "kafkaConsumerFactory")
    public ConsumerFactory<String, NotificationMessage> kafkaConsumerFactory() {
        return new DefaultKafkaConsumerFactory<>(
                kafkaConsumerConfigs(),
                new StringDeserializer(),
                new JsonDeserializer<>(NotificationMessage.class)
        );
    }

    @Bean(name = "kafkaListenerContainerFactory")
    public ConcurrentKafkaListenerContainerFactory<String, NotificationMessage> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, NotificationMessage> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(kafkaConsumerFactory());
        factory.getContainerProperties().setIdleEventInterval(Long.parseLong(consumerRetryIdleInterval));
        //factory.getContainerProperties().setAckMode(AbstractMessageListenerContainer.AckMode.MANUAL);
        factory.setConcurrency(consumerConcurrent);
        factory.setErrorHandler(new SerializationExceptionHandler());
        return factory;
    }

}
