package th.co.truecorp.iom.example.config;

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import th.co.truecorp.iom.example.producer.KafkaProducer;
import th.co.truecorp.iom.example.producer.OrderTypeKafkaProducer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaProducerConfig {

    @Value("${kafka.bootstrap.servers}")
    private String bootstrapServers;

    @Value("${kafka.ack:all}")
    private String kafkaAckCfg;

    @Value("${kafka.security.protocol:SSL}")
    private String kafkaSecurityProtocol;
    @Value("${kafka.security.truststore.location}")
    private String trustStoreLocation;
    @Value("${kafka.security.truststore.password}")
    private String trustStorePassword;
    @Value("${kafka.security.kerberos.service.name}")
    private String kafkaSecurityKerberosServiceName;
    @Value("${kafka.producer.max.block.ms:5000}")
    private Long maxBlockMs;

    @Bean(name = "trueQueueSenderProducerConfigs")
    public Map<String, Object> trueQueueSenderProducerConfigs() {
        Map<String, Object> props = new HashMap<>();

        if ("SASL_SSL".equals(kafkaSecurityProtocol)) {
            props.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, "SASL_SSL");
            props.put("sasl.mechanism", "GSSAPI");
            props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, trustStoreLocation);
            props.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, trustStorePassword);
            props.put("sasl.kerberos.service.name", kafkaSecurityKerberosServiceName);
        } else if ("SSL".equals(kafkaSecurityProtocol)) {
            props.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, "SSL");
            props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, trustStoreLocation);
            props.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, trustStorePassword);
        }

        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        // value to block, after which it will throw a TimeoutException
        props.put(ProducerConfig.MAX_BLOCK_MS_CONFIG, maxBlockMs);
        props.put(ProducerConfig.ACKS_CONFIG, kafkaAckCfg);
        return props;
    }

    @Bean(name = "kafkaProducerFactory")
    public ProducerFactory<Integer, String> kafkaProducerFactory() {
        return new DefaultKafkaProducerFactory<>(trueQueueSenderProducerConfigs());
    }

    @Bean(name = "kafkaTemplate")
    public KafkaTemplate<Integer, String> kafkaTemplate() {
        return new KafkaTemplate<>(kafkaProducerFactory());
    }

    @Bean
    public KafkaProducer kafkaProducer() {
        return new KafkaProducer();
    }

    //OrderType Producer
    @Bean(name = "OrderTopicProducerFactory")
    public ProducerFactory<Integer, String> OrderTopicProducerFactory() {
        return new DefaultKafkaProducerFactory<>(trueQueueSenderProducerConfigs());
    }

    @Bean(name = "OrderTopicKafkaTemplate")
    public KafkaTemplate<Integer, String> OrderTopicKafkaTemplate() {
        return new KafkaTemplate<>(kafkaProducerFactory());
    }

    @Bean
    public OrderTypeKafkaProducer orderTypeKafkaProducer() {
        return new OrderTypeKafkaProducer();
    }

}
