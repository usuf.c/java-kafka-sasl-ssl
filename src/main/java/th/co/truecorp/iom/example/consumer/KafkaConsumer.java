package th.co.truecorp.iom.example.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import th.co.truecorp.iom.example.model.NotificationMessage;
import th.co.truecorp.iom.example.producer.OrderTypeKafkaProducer;

import static net.logstash.logback.marker.Markers.append;

@Slf4j
@Component
public class KafkaConsumer {

    @Value("${iom.notification.order-type.topic}")
    String notificationTopic;

    @Autowired
    OrderTypeKafkaProducer kafkaProducer;

    @Autowired
    ObjectMapper objectMapper;

    @KafkaListener(
            topics = "${iom.notification.topic}",
            containerFactory = "kafkaListenerContainerFactory"
    )
    public void iomNotificationConsume(NotificationMessage notificationMessage) {

        String topic = "";
        try {

            String msg = objectMapper.writeValueAsString(notificationMessage);
            String orderType = notificationMessage.getOrderType();


            switch (orderType) {
                case "5501":
                    topic = notificationTopic.replaceAll("orderType", orderType)
                            .replaceAll("orderName", "registration");
                    break;
                case "5502":
                    topic = notificationTopic.replaceAll("orderType", orderType)
                            .replaceAll("orderName", "cancel-before-install");
                    break;
                case "5503":
                    topic = notificationTopic.replaceAll("orderType", orderType)
                            .replaceAll("orderName", "change-duedate");
                    break;
                default:
                    log.info("orderType not match");
            }

            kafkaProducer.send(
                    topic,
                    msg
            );

        } catch (JsonProcessingException e) {
            log.error("iomNotificationConsume exception", e);
        } finally {
            log.info(
                    append("iom-notification", notificationMessage).and(
                            append("produce-topic", topic)
                    ),
                    "receive iom-notification-message"
            );
        }

    }


}
