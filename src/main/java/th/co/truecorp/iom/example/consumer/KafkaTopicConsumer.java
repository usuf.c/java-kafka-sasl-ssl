package th.co.truecorp.iom.example.consumer;


import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import th.co.truecorp.iom.example.model.NotificationMessage;

import static net.logstash.logback.marker.Markers.append;

@Slf4j
@Component
public class KafkaTopicConsumer {

    @KafkaListener(
            topics = "uat03.iom-5501-registration",
            containerFactory = "kafkaListenerContainerFactory",
            id = "uat03.iom-5501-registration"
    )
    public void iomTopic5501NotificationConsume(NotificationMessage notificationMessage) {

        log.info(
                append("iom-topic-uat03-5501-notification", notificationMessage),
                "receive iom-topic-uat03-5501-notification-message"
        );

    }

    @KafkaListener(
            topics = "uat03.iom-5502-cancel-before-install",
            containerFactory = "kafkaListenerContainerFactory",
            id = "uat03.iom-5502-cancel-before -install"
    )
    public void iomTopic5502NotificationConsume(NotificationMessage notificationMessage) {

        log.info(
                append("iom-topic-uat03-5502-notification", notificationMessage),
                "receive iom-topic-uat03-5502-notification-message"
        );

    }

    @KafkaListener(
            topics = "uat03.iom-5503-change-duedate",
            containerFactory = "kafkaListenerContainerFactory",
            id = "uat03.iom-5503-change-duedate"
    )
    public void iomTopic5503NotificationConsume(NotificationMessage notificationMessage) {

        log.info(
                append("iom-topic-uat03-5503-notification", notificationMessage),
                "receive iom-topic-uat03-5503-notification-message"
        );

    }


}
