package th.co.truecorp.iom.example.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class NotificationMessage {
    @JsonProperty(value = "tracking_id")
    String trackingId;
    @JsonProperty(value = "channel")
    String channel;
    @JsonProperty(value = "create_ts")
    Date createTs;
    @JsonProperty(value = "order_id")
    String orderId;
    @JsonProperty(value = "order_status")
    String orderStatus;
    @JsonProperty(value = "order_type")
    String orderType;
}
