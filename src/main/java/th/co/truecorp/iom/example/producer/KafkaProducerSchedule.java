package th.co.truecorp.iom.example.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import th.co.truecorp.iom.example.model.NotificationMessage;

import java.util.Date;
import java.util.UUID;

@Slf4j
@Component
public class KafkaProducerSchedule {

    @Value("${iom.notification.topic}")
    String notificationTopic;

    @Autowired
    KafkaProducer kafkaProducer;

    @Autowired
    ObjectMapper objectMapper;

    @Scheduled(fixedRate = 2000)
    public void produceNotificationMessage() {

        try {

            //55001
            NotificationMessage notificationMessage = new NotificationMessage();
            notificationMessage.setTrackingId(UUID.randomUUID().toString());
            notificationMessage.setChannel("TEST-501");
            notificationMessage.setOrderId("ORDER-5501");
            notificationMessage.setOrderStatus("COMPLETED");
            notificationMessage.setOrderType("5501");
            notificationMessage.setCreateTs(new Date());

            String msg = objectMapper.writeValueAsString(notificationMessage);
            kafkaProducer.send(
                    notificationTopic,
                    msg
            );

            //55002
            notificationMessage = new NotificationMessage();
            notificationMessage.setTrackingId(UUID.randomUUID().toString());
            notificationMessage.setChannel("TEST-5502");
            notificationMessage.setOrderId("ORDER-5502");
            notificationMessage.setOrderStatus("COMPLETED");
            notificationMessage.setOrderType("5502");
            notificationMessage.setCreateTs(new Date());

            msg = objectMapper.writeValueAsString(notificationMessage);
            kafkaProducer.send(
                    notificationTopic,
                    msg
            );

            //55003
            notificationMessage = new NotificationMessage();
            notificationMessage.setTrackingId(UUID.randomUUID().toString());
            notificationMessage.setChannel("TEST-5503");
            notificationMessage.setOrderId("ORDER-5503");
            notificationMessage.setOrderStatus("COMPLETED");
            notificationMessage.setOrderType("5503");
            notificationMessage.setCreateTs(new Date());

            msg = objectMapper.writeValueAsString(notificationMessage);
            kafkaProducer.send(
                    notificationTopic,
                    msg
            );

        } catch (JsonProcessingException e) {
            log.error("produceNotificationMessage exception", e);
        } finally {

        }

    }

}

