package th.co.truecorp.iom.example.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.kafka.core.KafkaTemplate;

public class OrderTypeKafkaProducer {

    @Autowired
    @Qualifier("OrderTopicKafkaTemplate")
    private KafkaTemplate<Integer, String> kafkaTemplate;

    public void send(String topic, String message) {
        kafkaTemplate.send(topic, message);
    }

}
